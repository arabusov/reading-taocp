#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAXSTR 511
char buf[MAXSTR+2];
int buflen;

void init_input(char *input)
{
	int state = 0;
        assert(strlen(input) <= MAXSTR);
	strcpy(buf, input);
	buflen = strlen(buf);
	while (*input != '\0') {
		if (state == 0)
			if (*input == 'a')
				state = 1;
			else
				assert(0);
		if (state == 2)
			assert(*input == 'b');
		if (state == 1)
			if (*input == 'b')
				state = 2;
			else
				assert(*input == 'a');
		input++;
	}
	printf("Markov input: %s\n", buf);
}

void substitute(char *startp, int len, char *phi)
{
	int philen;
	char *p;
	philen = strlen(phi);
	assert(philen+buflen-len <= MAXSTR);
	assert(philen+buflen-len >= 0);
	if (len < philen) {
		char *from = startp + len;
		p = buf+buflen+philen-len-1;
		while (p != from+philen-len-1) {
			*p = *(p-philen+len);
			p--;
		}
	} else if (len > philen) {
		p = startp+philen;
		while (p != buf+buflen+philen-len) {
			*p = *(p+len-philen);
			p++;
		}
	}
	buflen = buflen + philen - len;
	buf[buflen] = '\0';
	p = startp;
	while (p < startp+philen) {
		*p++ = *phi++;
	}
}

int markov_rule(char *theta, char *phi)
{
	int len = strlen(theta);
	char tmp;
	int i;
	if (buflen < len)
		return 0;
	for (i = len; i < buflen+1; i++) {
		tmp = buf[i];
		buf[i] = '\0';
		if (0 == strcmp(buf+i-len, theta)) {
			buf[i] = tmp;
			substitute(buf+i-len, len, phi);
			return 1;
		}
		buf[i] = tmp;
	}
	return 0;
}

struct markov_record
{
	char *theta, *phi;
	int a;
	int b;
};

#define N 5
struct markov_record euclid_table[N] = {
	{ "ab", "", 2, 1 },
	{ "", "c", 0, 0 },
	{ "a", "b", 3, 2 },
	{ "c", "a", 4, 3 },
	{ "b", "b", 5, 0 }
};

void markov_algorithm(int step, struct markov_record *markov_table) {
	printf("Markov step: %d, state: %s\n", step, buf);
	if (step == N)
		return;
	if (markov_rule(markov_table[step].theta, markov_table[step].phi)) {
		markov_algorithm(markov_table[step].b, markov_table);
	} else {
		markov_algorithm(markov_table[step].a, markov_table);
	}
}

int main(int argc, char **argv)
{
	assert (argc == 2);
	init_input(argv[1]);
	markov_algorithm(0, euclid_table);
	printf("Markov output: %s\n", buf);
	return 0;
}

