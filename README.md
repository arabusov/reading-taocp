# Reading The Art of Computing Programming

My notes on "The Art of Computing Programming" (D. Knuth)

## Usage
In general, you should not use any code or info stored here unless you have really read and understood what you are going to do.

## License
MIT